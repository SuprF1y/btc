#config

uses env vars for congig

sample .env files in go/.env and php/.env

# go server
running at [http://198.199.107.176:8080/widgets/all](http://198.199.107.176:8080/widgets/all)

install [dep](https://github.com/golang/dep/releases) 

cd go/src/bigtincan

dep ensure

go build

./bigtincan

# php client

cd php

composer install

vendor/bin/phpunit  --bootstrap ./bootstrap.php --configuration test/phpunit.xml




